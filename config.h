//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/							/*Update Interval*/	/*Update Signal*/
	{"", 		"$HOME/bin/dwmblocks/getPacmanUpgradablePackages",		60,			1},
	{"", 		"$HOME/bin/dwmblocks/getHdd",                     		60,			12},
	{"", 		"$HOME/bin/dwmblocks/getSysload",				60,			11},
	{"", 		"$HOME/bin/dwmblocks/getMem",					60,			2},
	{"", 		"$HOME/bin/dwmblocks/getNetworking",				60,			3},
	{"", 		"$HOME/bin/dwmblocks/getWireguardState",			60,			4},
	/*{"", 		"$HOME/bin/dwmblocks/getExternalip",				60,			5},*/
	{"", 		"$HOME/bin/dwmblocks/getVolume",				0,			6},
	{"", 		"$HOME/bin/dwmblocks/getBluetooth",				60,			7},
	{"",		"$HOME/bin/dwmblocks/getBat",					60,			8},
	{"",		"$HOME/bin/dwmblocks/getWeather",				60,			9},
	{"",		"$HOME/bin/dwmblocks/getDatetime",				60,			10},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ' ';

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:

// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
